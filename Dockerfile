FROM nginx:latest
ADD https://gitlab.com/m.timoir/new_project/-/raw/main/index.html /usr/share/nginx/html/
RUN chmod +r /usr/share/nginx/html/index.html
CMD ["nginx", "-g", "daemon off;"]
EXPOSE 80
